use std::ops;

/// A structure for using matrix
#[derive(Clone, Debug)]
pub struct Matrix {
    rows: usize,
    columns: usize,
    content: Vec<f64>,
}

impl Matrix {
    pub fn new(rows: usize, columns: usize) -> Matrix {
        Matrix {
            rows,
            columns,
            content: vec![0.0; rows * columns],
        }
    }

    pub fn from(rows: usize, columns: usize, v: Vec<f64>) -> Matrix {
        let mut content = vec![0.0; rows * columns];
        let l = if content.len() > v.len() {
            v.len()
        } else {
            content.len()
        };
        for i in 0..l {
            content[i] = v[i]
        }
        Matrix {
            rows,
            columns,
            content,
        }
    }

    pub fn size(&self) -> usize {
        self.rows * self.columns
    }

    pub fn rows(&self) -> usize {
        self.rows
    }

    pub fn columns(&self) -> usize {
        self.columns
    }

    pub fn determinant(&self) -> f64 {
        if self.rows != self.columns {
            panic!("Determinant exist only for (N x N) matrix");
        }

        if self.rows == 2 {
            // ad - bc
            (self.content[0] * self.content[3]) - (self.content[1] * self.content[2])
        } else {
            let mut det = 0.0;
            let sign: i8 = -1;
            let mut submatrix = Matrix::new(self.rows - 1, self.columns - 1);

            for x in 0..self.rows {
                let mut subi = 0;
                for i in 1..self.rows {
                    let mut subj = 0;
                    for j in 0..self.rows {
                        if j != x {
                            submatrix.set_at(subi, subj, self.get_at(i, j));
                            subj += 1;
                        }
                    }
                    subi += 1;
                }
                det += sign.pow(x as u32) as f64 * self.get_at(0, x) * submatrix.determinant();
            }
            det
        }
    }

    fn get_index(&self, row: usize, column: usize) -> usize {
        row * self.columns + column
    }

    fn get_at(&self, row: usize, column: usize) -> f64 {
        self[self.get_index(row, column)]
    }

    fn set_at(&mut self, row: usize, column: usize, value: f64) -> () {
        let index = self.get_index(row, column);
        self[index] = value
    }
}

impl ToString for Matrix {
    fn to_string(&self) -> String {
        let mut output = String::from(format!(
            "[{} x {}] : {}",
            self.rows,
            self.columns,
            self.size()
        ));

        for (i, value) in self.content.iter().enumerate() {
            if i % &self.columns == 0 {
                output.push('\n');
            }
            let fmt_value = format!("{: <6}", value);
            output.push_str(fmt_value.as_str());
        }
        output
    }
}

impl ops::Add<Matrix> for Matrix {
    type Output = Matrix;

    fn add(self, rhs: Matrix) -> Matrix {
        if self.rows != rhs.rows && self.columns != rhs.columns {
            panic!("Matrixes must have the same rows and columns.");
        } else {
            let mut result = self.clone();

            for i in 0..self.size() {
                result[i] = rhs[i] + self.content[i];
            }

            result
        }
    }
}

impl ops::Add<&Matrix> for &Matrix {
    type Output = Matrix;

    fn add(self, rhs: &Matrix) -> Matrix {
        if self.rows != rhs.rows && self.columns != rhs.columns {
            panic!("Matrixes must have the same rows and columns.");
        } else {
            let mut result = self.clone();

            for i in 0..self.size() {
                result[i] = rhs[i] + self.content[i];
            }

            result
        }
    }
}

impl ops::Add<Matrix> for &mut Matrix {
    type Output = Matrix;

    fn add(self, rhs: Matrix) -> Matrix {
        if self.rows != rhs.rows && self.columns != rhs.columns {
            panic!("Matrixes must have the same rows and columns.");
        } else {
            let mut result = self.clone();

            for i in 0..self.size() {
                result[i] = rhs[i] + self.content[i];
            }

            result
        }
    }
}

impl ops::AddAssign<Matrix> for Matrix {
    fn add_assign(&mut self, rhs: Matrix) {
        if self.rows != rhs.rows && self.columns != rhs.columns {
            panic!("Matrixes must have the same rows and columns.");
        } else {
            for i in 0..self.size() {
                self[i] = rhs[i] + self[i];
            }
        }
    }
}

impl ops::Mul<Matrix> for Matrix {
    type Output = Matrix;

    fn mul(self, rhs: Matrix) -> Matrix {
        if self.columns != rhs.rows {
            panic!("Second matrix must have the same columns as the first matrix rows.");
        } else {
            let mut result = self.clone();

            for i in 0..self.rows {
                for j in 0..rhs.columns {
                    let mut value = 0.0;
                    for k in 0..self.columns {
                        value += self.get_at(i, k) * rhs.get_at(k, j);
                    }

                    result.set_at(i, j, value);
                }
            }

            result
        }
    }
}

impl ops::Mul<&Matrix> for &Matrix {
    type Output = Matrix;

    fn mul(self, rhs: &Matrix) -> Matrix {
        if self.columns != rhs.rows {
            panic!("Second matrix must have the same columns as the first matrix rows.");
        } else {
            let mut result = self.clone();

            for i in 0..self.rows {
                for j in 0..rhs.columns {
                    let mut value = 0.0;
                    for k in 0..self.columns {
                        value += self.get_at(i, k) * rhs.get_at(k, j);
                    }

                    result.set_at(i, j, value);
                }
            }

            result
        }
    }
}

impl ops::MulAssign<Matrix> for Matrix {
    fn mul_assign(&mut self, rhs: Matrix) {
        if self.columns != rhs.rows {
            panic!("Second matrix must have the same columns as the first matrix rows.");
        } else {
            let mut result = self.clone();

            for i in 0..self.rows {
                for j in 0..rhs.columns {
                    let mut value = 0.0;
                    for k in 0..self.columns {
                        value += self.get_at(i, k) * rhs.get_at(k, j);
                    }

                    result.set_at(i, j, value);
                }
            }

            for i in 0..result.size() {
                self[i] = result[i];
            }
        }
    }
}

impl ops::Mul<f64> for Matrix {
    type Output = Matrix;

    fn mul(self, rhs: f64) -> Matrix {
        let mut result = self.clone();

        for i in 0..self.size() {
            result[i] = self[i] * rhs;
        }

        result
    }
}

impl ops::MulAssign<f64> for Matrix {
    fn mul_assign(&mut self, rhs: f64) {
        for i in 0..self.size() {
            self.content[i] *= rhs;
        }
    }
}

impl PartialEq for Matrix {
    fn eq(&self, rhs: &Matrix) -> bool {
        self.content == rhs.content
    }
}

impl ops::Index<usize> for Matrix {
    type Output = f64;
    /// Return Matrix value at [position]
    /// If [position] is greater then the content - the last element will be returned
    fn index(&self, position: usize) -> &Self::Output {
        let max = self.content.len() - 1;
        let index = if position > max { max } else { position };
        &self.content[index]
    }
}

impl ops::IndexMut<usize> for Matrix {
    /// Return a mutable value of Matrix at [position]
    /// If [position] is greater then the content - the last element will be returned
    fn index_mut(&mut self, position: usize) -> &mut f64 {
        let max = self.content.len() - 1;
        let index = if position > max { max } else { position };
        &mut self.content[index]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        let m = Matrix::from(2, 2, vec![1.0, 2.0, 3.0, 4.0]);
        let n = Matrix::from(2, 2, vec![1.0, 1.0, 1.0, 1.0]);
        let t = Matrix::from(2, 2, vec![2.0, 3.0, 4.0, 5.0]);

        assert_eq!(t, m + n);

        let m = Matrix::from(3, 3, vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]);
        let n = Matrix::from(3, 3, vec![1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]);
        let t = Matrix::from(3, 3, vec![2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]);

        assert_eq!(t, m + n);
    }

    #[test]
    fn test_mul() {
        let m = Matrix::from(2, 2, vec![1.0, 2.0, 3.0, 4.0]);
        let n = Matrix::from(2, 2, vec![1.0, 0.0, 0.0, 1.0]);

        assert_eq!(m.clone(), m * n);

        let m = Matrix::from(3, 2, vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0]);
        let n = Matrix::from(2, 2, vec![1.0, 0.0, 0.0, 1.0]);

        assert_eq!(m.clone(), m * n);
    }

    #[test]
    fn test_mul_scalar() {
        let m = Matrix::from(2, 2, vec![1.0, 2.0, 3.0, 4.0]);
        let n = Matrix::from(2, 2, vec![2.0, 4.0, 6.0, 8.0]);

        assert_eq!(n, m * 2.0);

        let m = Matrix::from(3, 3, vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]);
        let n = Matrix::from(3, 3, vec![2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0]);

        assert_eq!(n, m * 2.0);
    }

    #[test]
    fn test_determinant() {
        let m = Matrix::from(2, 2, vec![1.0, 2.0, 3.0, 4.0]);

        assert_eq!(m.determinant(), -2.0);

        let m = Matrix::from(2, 2, vec![4.0, 2.0, -2.0, 8.0]);

        assert_eq!(m.determinant(), 36.0);

        let m = Matrix::from(3, 3, vec![4.0, 5.0, 6.0, 1.0, 2.0, 3.0, 5.0, 3.0, 1.0]);

        assert_eq!(m.determinant(), 0.0);
    }
}
