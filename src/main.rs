mod matrix;

use std::io;
use std::io::Write;

macro_rules! print_flush {
    ($($arg:tt)*) => {
        print!($($arg)*);
        io::stdout().flush().unwrap();
    };
}

struct App {
    input_numbers: Vec<f64>,
}

impl App {
    pub fn new() -> App {
        App {
            input_numbers: vec![],
        }
    }

    pub fn run(&mut self) {
        let mut op = -1;
        let version = env!("CARGO_PKG_VERSION");

        println!("Welcome to rust Matrix V{} - by David Delarosa", version);
        println!("____________________________________________________\n");

        while op != 0 {
            println!("Select from the following:");
            println!("1 - Multiply matrix");
            println!("2 - Calculete matrix determinant");
            println!("3 - Sum matrix together");
            println!("4 - Multiply matrix in scalar");
            println!("0 - Exit");

            print_flush!("Selection: ");
            op = self.get_number() as i16;
            match op {
                1 => self.mult(),
                2 => self.determinant(),
                3 => self.sum(),
                4 => self.scalar(),
                0 => println!("See you later."),
                _ => {}
            }
        }
    }

    fn get_number(&mut self) -> f64 {
        loop {
            match self.input_numbers.pop() {
                Some(number) => return number,
                None => {
                    let mut op = String::new();
                    io::stdin().read_line(&mut op).expect("Could not read line");
                    let mut numbers: Vec<f64> = op
                        .trim()
                        .split_whitespace()
                        .filter_map(|val| val.parse().ok())
                        .collect();
                    self.input_numbers.append(&mut numbers);
                }
            }
        }
    }

    fn init_matrix(&mut self, mat: &mut matrix::Matrix) -> () {
        for i in 0..mat.size() {
            mat[i] = self.get_number();
        }
    }

    fn mult(&mut self) -> () {
        println!("Matrix Multiplication");
        println!("____________________________________________________\n");

        let number: usize;
        let rows: usize;
        let mut columns: usize;

        print_flush!("Enter the number to multiply: ");
        number = self.get_number() as usize;

        println!("Enter first matrix size (N x K): ");
        rows = self.get_number() as usize;
        columns = self.get_number() as usize;

        let mut result = matrix::Matrix::new(rows, columns);
        println!("Enter {} values:", result.size());
        self.init_matrix(&mut result);

        for i in 1..number {
            print_flush!("Enter matrix #{} columns: ", i + 1);
            columns = self.get_number() as usize;
            let mut temp = matrix::Matrix::new(result.columns(), columns);
            println!("Enter {} values for matrix #{}: ", temp.size(), i + 1);
            self.init_matrix(&mut temp);
            result = result * temp;
        }

        println!("The result is: {}", result.to_string());
    }

    fn determinant(&mut self) -> () {
        println!("Matrix Determinant Calculator");
        println!("____________________________________________________\n");

        let rows: usize;

        print_flush!("Enter matrix size (N x N): ");
        rows = self.get_number() as usize;

        let mut result = matrix::Matrix::new(rows, rows);
        println!("Enter {} values: ", result.size());

        self.init_matrix(&mut result);

        println!(
            "The determinant of the matrix\n{}\nis: {}",
            result.to_string(),
            result.determinant()
        );
    }

    fn sum(&mut self) -> () {
        println!("Matrix Summerize");
        println!("____________________________________________________\n");

        let number: usize;
        let rows: usize;
        let cols: usize;

        print_flush!("Enter the number to sum: ");
        number = self.get_number() as usize;

        println!("Enter first matrix size (N x K):");
        rows = self.get_number() as usize;
        cols = self.get_number() as usize;

        let mut result = matrix::Matrix::new(rows, cols);
        println!("Enter {} values:", result.size());
        self.init_matrix(&mut result);

        for i in 1..number {
            let mut temp = matrix::Matrix::new(rows, cols);
            println!("Enter {} values for matrix #{}:", temp.size(), i + 1);
            self.init_matrix(&mut temp);

            result += temp;
        }

        println!("The result is:\n{}", result.to_string());
    }

    fn scalar(&mut self) -> () {
        println!("Matrix Scalar Multiplication");
        println!("____________________________________________________\n");

        let rows: usize;
        let cols: usize;
        let value: f64;

        println!("Enter first matrix size (N x K):");
        rows = self.get_number() as usize;
        cols = self.get_number() as usize;

        let mut result = matrix::Matrix::new(rows, cols);
        println!("Enter {} values:", result.size());
        self.init_matrix(&mut result);

        print_flush!("Enter a scalar value to multiply with: ");
        value = self.get_number();

        println!(
            "The reslut of the matrix:\n{}\n times {} is:\n{}",
            result.to_string(),
            value,
            (result * value).to_string()
        );
    }
}

fn main() {
    let mut app = App::new();
    app.run();
}
